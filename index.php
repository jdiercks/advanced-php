<?php

	//storing ten names in a variable
	$names = array("John", "Alex", "Jeff", "Joe", "Jimmy", "Ali", "Brittny", "Sara", "Carin", "Rachel");

	//for loop to loop through the names array
	$namesLength = count($names);

	//storing random number between 0 and 20 in a variable
	$rand = rand(0,20);
?>
<!DOCTYPE html>
<html>
<head>
	<title>
	Advanced PHP week 1
	</title>
</head>
<body>

	<?php
	//if rand less than or equal to 9
	if($rand <= 9){

	?>

		<!--out whatever index the array lands on in between 0 and 9-->
		<p>Hello <?php echo $names[$rand] ?></p>

	<?php

	}
	else{

		?>

		<p>Names List:</p>

		<?php

		//else echo out the entire names array in order

		for($i = 0; $i < $namesLength; $i++){
			
			echo $names[$i];
			echo "<br>";

		}

	}

	?>

	<!--used to print out the random number and make sure working right-->
	<p>Current Number: <?php echo $rand ?></p> 


</body>
</html>